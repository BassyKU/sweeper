#include <iostream>
#include <ncurses/curses.h>
#include "ChoiceTable.h"
#include "Sweeper.h"
using namespace std;

int main() {
        ChoiceTable c;
        c.welcome();
        c.out(5); c.out(10); c.out(20); c.out(50); c.out(100);
	c.final();  c.in();

        Sweeper sgame;
        int START_X=0,START_Y=0;
        int GAME_SIZE,GAME_BOOM;
//      cout<<"start(x y)= ";
//      cin>>START_X>>START_Y;
        sgame.setStartPoint(START_X,START_Y);

//        cout<<"plz keyin size:";
        GAME_SIZE=c.getGameSize();
//        cout<<"plz keyin bombs:";
        GAME_BOOM=c.getGameBoom();
        sgame.setGameNum(GAME_SIZE,GAME_BOOM);

        initscr();
        cbreak();       //disable key buffering
        noecho();       //disable echoing
        keypad(stdscr, TRUE);   //enable keypad reading
//        nodelay(stdscr,TRUE);
        sgame.setBoom();
        sgame.menu();
        sgame.moveKeypad();

        endwin();
        return 0;
} //int main()

