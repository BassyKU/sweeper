#include <ncurses/curses.h>
#include <ctime>	//for time_t and time()
#include <unistd.h>	//for sleep()
#include <cstdlib>	//for srand and rand()
#include "CountTime.h"
#include "SubWindow.h"
#include "FileIO.h"

#define BIGNUM 1000

class Sweeper{
public:
        void menu(){
                flag=0; hint=0; menuX=x0+G_Size+12; menuY=y0;
                mvprintw(menuY+1,menuX,"Now Flags Number:%3d\n",flag);
                mvprintw(menuY+2,menuX,"Press 'TAB' to see the answer(3 sec.)\n");
                mvprintw(menuY+3,menuX,"Press 'g' to open the grid\n");
                mvprintw(menuY+4,menuX,"Press 'f' to set a flag\n");
                mvprintw(menuY+5,menuX,"Press 'h' for a hint!\n");
                mvprintw(menuY+6,menuX,"Press 'q' to quit\n");
        } //void menu()
        void moveKeypad(){
                go_on=TRUE;
                x=x0+1; y=y0+1;
                ct.timeWin(menuY+0,menuX);
//                c=getch();
//                if(c=='s') {
//              nodelay(stdscr,TRUE);
                while(go_on){
                        ct.timeCounter();
                        refresh();
                        move(y,x);
                        c=getch();
                        switch(c){
                                case KEY_LEFT:  x--;
	                                        mvaddstr(menuY+8,menuX,"LEFT ");
                                                break;
                                case KEY_RIGHT: x++;
                                                mvaddstr(menuY+8,menuX,"RIGHT");
                                                break;
                                case KEY_UP:    y--;
                                                mvaddstr(menuY+8,menuX,"UP   ");
                                                break;
                                case KEY_DOWN:  y++;
                                                mvaddstr(menuY+8,menuX,"DOWN ");
                                                break;
                                case 'g':       pressG();       break;
                                case 'f':       pressF();       break;
                                case 'h':       pressH();       break;
                                case 'q':       pressQ();       break;
                                case '\t':      pressTAB();     break;
                                default:        go_on=TRUE;
                        } //switch(c)
                        while(x<=x0)            x+=G_Size;      //over left(jump to right)
                        while(x>=x0+G_Size+1)   x-=G_Size;      //over right(jump to left)
                        while(y<=y0)            y+=G_Size;      //over up(jump to down)
                        while(y>=y0+G_Size+1)   y-=G_Size;      //over down(jump to up)
			checkBOOM();
                } //while(go_on)
//                } //if(c='s')
        } //void moveKeypad()
	void pressT(){ /***input SweeperTemp***/
		temp.inTemp(G_Size,bla);
		final.outTemp(G_Size,boom);
	} //void pressT()
        void pressS(){	/***save SweeperTemp***/
		temp.outTemp(G_Size,bla);
		final.outTemp(G_Size,boom);
        } //void pressS()
        void pressQ(){
                go_on=FALSE;
        } //void pressQ()
        void pressG(){
                i=y*(G_Size+2)+x;
		switch(bla[i]){
			case 'b': bla[i]='f'; mvaddch(y,x,'f'); break;
			case 'f': bla[i]='f'; mvaddch(y,x,'f'); break;
			default : switch(boom[i]){
					case 'b':
	                        		bla[i]='b'; mvaddch(y,x,'b');
						GGlost();	break;
		                        default:bla[i]=boom[i];
						mvaddch(y,x,bla[i]);
//						findAround(y,x,boom[i]);
				} //switch(boom[i])
		} //switch(bla[i])
        } //void pressG()
	void checkBOOM(){
		int B_Check=0;
                for(j=0;j<sum;j++){
                        if(boom[j]=='b' && (bla[j]==32 || bla[j]=='f'))	B_Check++;	
			if(boom[j]!='b' && (bla[j]==32 || bla[j]=='f'))	B_Check=0;	
                } //for(j=0;j<sum;j++)
                if(B_Check==B_Num) GGwin();
	} //void checkBOOM()
	void GGlost(){
		int line=10;	//the number of skipping line
		beep(); sleep(1);
		mvprintw(menuY+line+0,menuX,"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                mvprintw(menuY+line+1,menuX,"^           Oops GG!!0.0            ^");
                mvprintw(menuY+line+2,menuX,"^        GAME OVER~ YOU LOST        ^");
                mvprintw(menuY+line+3,menuX,"^ the system will end after  5 sec. ^");
                mvprintw(menuY+line+4,menuX,"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                refresh();
                solu.callSubWin();
		final.outResult(G_Size,boom);
                sleep(5);
                go_on=FALSE;
	} //void GGlost
	void GGwin(){
		int line=10;	//the number of skipping line
		mvprintw(menuY+line+0,menuX,"*************************************");
		mvprintw(menuY+line+1,menuX,"*           Congratulation          *");
		mvprintw(menuY+line+2,menuX,"*          YOU WIN THE GAME         *");
		mvprintw(menuY+line+3,menuX,"* the system will end after 10 sec. *");
		mvprintw(menuY+line+4,menuX,"*************************************");
		refresh();
		solu.callSubWin();
		final.outResult(G_Size,boom);
		sleep(10);
		go_on=FALSE;
	} //void GGwin()
        void findAround(int ay,int ax,char num){
                wl=G_Size+2;
                j = ay*wl+ax;
                if(bla[j+1]==32 && boom[j+1]==num) {
                        mvaddch(y,x+1,bla[j+1]=boom[j+1]);
                        findAround(y,x+1,num);
                } if(bla[j-1]==32 && boom[j-1]==num) {
                        mvaddch(y,x-1,bla[j-1]=boom[j-1]);
                        findAround(y,x-1,num);
                } if(bla[j+wl]==32 && boom[j+wl]==num) {
                        mvaddch(y+1,x,bla[j+wl]=boom[j+wl]);
                        findAround(y+1,x,num);
                } if(bla[j-wl]==32 && boom[j-wl]==num) {
                        mvaddch(y-1,x,bla[j-wl]=boom[j-wl]);
                        findAround(y-1,x,num);
                } if(bla[j+1+wl]==32 && boom[j+1+wl]==num) {
                        mvaddch(y+1,x+1,bla[j+1+wl]=boom[j+1+wl]);
                        findAround(y+1,x+1,num);
                } if(bla[j+1-wl]==32 && boom[j+1-wl]==num) {
                        mvaddch(y-1,x+1,bla[j+1-wl]=boom[j+1-wl]);
                        findAround(y-1,x+1,num);
                } if(bla[j-1+wl]==32 && boom[j-1+wl]==num) {
                        mvaddch(y+1,x-1,bla[j-1+wl]=boom[j-1+wl]);
                        findAround(y+1,x-1,num);
                } if(bla[j-1-wl]==32 && boom[j-1-wl]==num) {
                        mvaddch(y-1,x-1,bla[j-1-wl]=boom[j-1-wl]);
                        findAround(y-1,x-1,num);
                } //if(chain rule)
        } //void findAround()
        void pressTAB(){
                solu.callSubWin();
//              getch();        //key anykey to close the subwindow
                sleep(3);       //stop for 3 seconds
                touchwin(stdscr);
        } //void pressTAB()
        void pressH(){
                j=TRUE;
                mvprintw(menuY+5,menuX+22,"Hint has been used:%3d",++hint);
                srand(time(NULL));
                while(j==TRUE && hint<B_Num){
                        r = rand()%sum;
                        if(bla[r]==32 && boom[r]=='b') {
//                              start_color();
//                              init_pair(0,COLOR_RED,COLOR_WHITE);
//                              attron(COLOR_PAIR(0));
                                mvaddch(y0+r/wl, x0+r%wl, bla[r]='b');
                                refresh();
                                j=FALSE;
//                              attroff(COLOR_PAIR(0));
                        } else if(bla[r]=='b' && boom[r]=='b') {
                                j=TRUE;
                        } //if-else(boom[r]=='b')
		} //while(j==TRUE && hint<B_Num)
		while(hint>=B_Num){
				hint=B_Num;
                                mvprintw(menuY+11,menuX,"Hint been used all~~");
				hint--;
                } //while(hint>=B_Num)
        } //void pressH()
      void pressF(){
                i=y*(G_Size+2)+x;
                switch(bla[i]) {
                        case 'f': bla[i]=32;    flag--; break;  //32=[space]
                        default : bla[i]='f';   flag++; break;
                } //switch(bla[yi*(G_Size+2)+xi])
                mvaddch(y,x,bla[i]);
                mvprintw(menuY+1,menuX,"Now Flags Number:%3d\n",flag);
        } //void pressF()
        void setBoom(){
                wl = G_Size+2;  //wall length
                sum = wl*wl;    //game area(include wall)
                solu.newSubWin(wl,wl,y0,x0);
                for (i=0;i<sum;i++) {
                        boom[i]='0'; bla[i]='0';
                } //for (int i=0;i<sum;i++)
                srand(time(NULL));      //before making random number
                for (j=0;j<B_Num;j++) { /***for setting bombs***/
                        r = rand()%sum; //random number of time
                        if(r<(wl-1) || (r<sum && r>(sum-wl)) || r%wl==0 || (r+1)%wl==0) j--;
                        else {
                                switch(boom[r]) {
                                        case '0': boom[r]='b'; break;
                                        default : j--;
                                } //switch(boom[rand()%sum])
                        } //else
                } //for(int j=0;j<B_Num;j++) /***for setting bombs***/
                for (i=0;i<sum;i++) {   /***for the body of boom[]***/
                        bla[i]=32;      //ASCII code of 'Space' is 32.
                        switch(boom[i]) {       //count around bombs
                                case 'b': break;
                                default :
                                        if(boom[i+1]=='b') boom[i]++;    //right
                                        if(boom[i-1]=='b') boom[i]++;    //left
                                        if(boom[i+1+wl]=='b') boom[i]++; //right dwon
                                        if(boom[i-1+wl]=='b') boom[i]++; //left dwon
                                        if(boom[i+1-wl]=='b') boom[i]++; //right up
                                        if(boom[i-1-wl]=='b') boom[i]++; //left up
                                        if(boom[i+wl]=='b') boom[i]++;   //up
                                        if(boom[i-wl]=='b') boom[i]++;   //down
                        } //switch(boom[i])
                        if(i<(wl-1) || (i<sum && i>(sum-wl)) || i%wl==0 || (i+1)%wl==0) {
                                boom[i]='*';    //wall shape
                                bla[i]='*';
                        } //if(i is at the wall)
                        solu.setSubWin(y0+i/wl, x0+i%wl, boom[i]);
                        mvaddch(y0+i/wl, x0+i%wl, bla[i]);      //draw the map
                } //for(int i=0;i<sum;i++) /***for the body of boom[]***/
        } //void setBoom(i)
        void setStartPoint(int START_X,int START_Y){
                x0 = START_X;
                y0 = START_Y;
        } //void setStartPoint()
        void setGameNum(int GAME_SIZE,int GAME_BOOM){
                G_Size = GAME_SIZE;
                B_Num = GAME_BOOM;
        } //void setGameNum()
private:
        CountTime ct;
        SubWindow solu;
	FileIO final;
	FileIO temp;
        int i,j,r;
        int x,y,x0,y0,menuX,menuY;
        int G_Size,B_Num,wl,sum;
        char boom[BIGNUM];
        char bla[BIGNUM];
        int go_on,c,flag,hint;
}; //class Sweeper

