#include <iostream>
#include <fstream>      //file stream
#include <cstdlib>      //exit(1)
using namespace std;

class FileIO{
public:
        void outResult(int size, char array[]){
                wall=size+2;
                sum=wall*wall;
                OutResult.open("SweeperResult.txt",ios::out);   //output file
                if (!OutResult){
                        cerr<<"File could not be opened"<<endl;
                        exit(1);
                } //if (!OutResult)
                for(int i=0;i<sum;i++){
                        if(i>wall && i<(sum-wall) && (i%wall)!=0 && (i%wall)!=(wall-1))
                                OutResult << array[i];
                        if((i%wall)==(wall-1) && i>wall)
                                OutResult << endl;
                } //for(int i=0;i<sum;i++)
                OutResult.close();      //destructor; to close the opening file
        } //void outResult(int size, char array[])
        void outTemp(int size, char array[]){
                wall=size+2;
                sum=wall*wall;
                OutTemp.open("SweeperTemp.txt",ios::out);       //output file
                if (!OutTemp){
                        cerr<<"File could not be opened"<<endl;
                        exit(1);
                } //if (!OutTemp)
                for(int i=0;i<sum;i++){
//                      if(i>wall && i<(sum-wall) && (i%wall)!=0 && (i%wall)!=(wall-1))
                        OutTemp<<array[i];
//                      if((i%wall)==(wall-1))
//                              OutTemp<<endl;
                } //for(int i=0;i<sum;i++)
                OutTemp.close();        //destructor; to close the opening file
        } //void outTemp(int size, char array[])
        void inTemp(int size, char array[]) {
                InTemp.open("SweeperTemp.txt",ios::in); //input file
                if (!InTemp){
                        cerr<<"File could not be opened"<<endl;
                        exit(1);
                } //if (!InTemp)
                char t;
                int i=0;
		wall=size+2;
                sum=wall*wall;
                while(!InTemp.eof() || i<sum) {//you can also write:while(InTemp>>t!=EOF
                        InTemp.get(t);  //get a char                    array[i++]=t;
                        array[i++]= t;
                } //while(InTemp>>t != EOF)
                InTemp.close();
        } //void inTemp(int maxsize, char array[])

private:
        ofstream OutResult;
        ofstream OutTemp;
        ifstream InTemp;
        int wall,sum;
}; //class FileIO

