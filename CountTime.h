#include<ctime>         //for time_t and time()
#include<unistd.h>      //for sleep()
#include<ncurses/curses.h>
class CountTime{
public:
	void timeWin(int yt,int xt){
		x=xt; y=yt;
//		tWin=newwin(1,15,y,x);
//		touchwin(tWin);
		t1=time(NULL);  //get elapsed seconds since 1970/1/1 00:00:00
	} //void timeWin(int yt,int xt)
        void timeCounter(){
                time(&t2);      //you can also pass a pointer to time()
	        diff_t=difftime(t2,t1);
		t_sec=(int)diff_t%60;
		t_min=(int)(diff_t/60.0)%60;
		t_hr=(int)(diff_t/60.0/60.0)%24;
		mvprintw(y,x,"Time: %2.0f:%2.0f:%2.0f\n",t_hr,t_min,t_sec);
//                mvwprintw(tWin,0,0,"Time: %2.0f:%2.0f:%2.0f\n",t_hr,t_min,t_sec); 
//                sleep(1);        //sleep for 1 seconds
//                nodelay(tWin,TRUE);
//		wrefresh(tWin);
		nodelay(stdscr,TRUE);
//		refresh();
        } //void timeCounter()
private:
        time_t t1,t2;
	double diff_t,t_sec,t_min,t_hr;
	int x,y;
	WINDOW* tWin;
};

