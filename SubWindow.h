/*SubWindow.h*/
#include <ncurses/curses.h>

class SubWindow{
public:
        void newSubWin(int row,int col,int startY,int startX){
                win=newwin(row,col,startY,startX);
        } //void setSubWin(()
        void setSubWin(int row,int col,char ch){
                mvwaddch(win,row,col,ch);     //draw the map
        } //void setSolWin()
        void callSubWin(){
                touchwin(win);        //need do 'touchwin()' before 'wrefresh()'
                wrefresh(win);
        } //void callSolWin()

private:
	WINDOW* win;

}; //class SetSubWin
