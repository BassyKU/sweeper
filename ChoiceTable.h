#include <iostream>
#include <ncurses/curses.h>
using namespace std;
#define OP 20

class ChoiceTable{
public:
        void welcome(){
                cout<<"Welcome to Minesweeper! Please Choose the game size:"<<endl;
                i=0;
		for(int j=0;j<OP;j++)	{option[j][0]=0; option[j][1]=0;}
        } //Welcome
        void out(int b){
                cout<<++i<<") ";        //i++: output the original i first, then add 1
                                        //++i: add 1 first, then output i
                if(b<50)                {cout<<"Small ";	option[i][0]=10;}
                else if(b>=50 && b<100) {cout<<"Medium ";	option[i][0]=20;}
                else if(b>=100 && b<200){cout<<"Large ";	option[i][0]=30;}
                else                    {cout<<"Devil ";	option[i][0]=50;}
                cout<<"level with "<<b<<" Bombs"<<endl;
		option[i][1]=b;
        } //Out
        void final(){
                cout<<++i<<") ";
                cout<<"Other level(customize game-size & bomb-number)"<<endl;
        } //Final
        void in(){
                cout<<"=>choose option:";
                cin>>keyin;
                if (keyin==i) { //choose 'other size'
                        cout<<"input size:";
                        cin>>gamesize;
                        cout<<"input bombs:";
                        cin>>gameboom;
                } else if (keyin<i) {
                        gamesize=option[keyin][0];
                        gameboom=option[keyin][1];
                } else {
			cout<<"error!"<<endl;
			in();
		}//if-else(keyin)
        } //In
	int getGameSize(){
		return gamesize;
	}
	int getGameBoom(){
		return gameboom;
	}
private:
        int i,keyin;
        int option[OP][2];
        int gamesize,gameboom;
};
